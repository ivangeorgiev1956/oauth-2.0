require("dotenv").config();

const express = require("express");
const cors = require("cors");

const port = process.env.PORT || 8001;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

const app = express();

app.set('view engine', 'ejs');

app.use(cors());

app.listen(port, function () {
    console.log("Listening on port " + port);
});