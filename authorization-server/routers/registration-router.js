const express = require("express");

module.exports = function (clientService) {
    const router = express.Router();

    router.get("", function (request, response) {
        response.render('register');
    });

    router.post("", function (request, response) {
        const { name, description, redirection_url: redirectionUrl } = request.body;

        const client = clientService.registerClient(name, description, redirectionUrl);

        response.render('register-success', { client });
    });

    return router;
};