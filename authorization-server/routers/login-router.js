const express = require("express");

module.exports = function (userService) {
    const router = express.Router();

    router.get("", function (request, response) {
        response.render('login');
    });

    router.post("", function (request, response) {
        const { username, password } = request.body;

        if (userService.areCredentialsValid(username, password)) {
            return response.sendStatus(200);
        }

        response.sendStatus(400);
    });

    return router;
};