const express = require("express");

module.exports = function (clientService, userService, authorizationCodeService) {
    const router = express.Router();

    router.get("", function (request, response) {
        const { response_type: responseType, client_id: clientId, user_id: userId, scope } = request.query;

        if (responseType !== "code") {
            return response.sendStatus(400);
        }

        const client = clientService.findById(clientId);

        if (!client) {
            return response.sendStatus(400);
        }

        const { name: clientName, description: clientDescription } = client;

        return response.render('authorize', { authorizationRequest: { clientId, clientName, clientDescription, userId, scope } });
    });

    router.post("", function (request, response) {
        const { client_id: clientId, user_id: userId, scope } = request.body;

        const client = clientService.findById(clientId);

        if (!client) {
            return response.sendStatus(400);
        }

        const user = userService.findById(userId);

        if (!user) {
            return response.sendStatus(400);
        }

        const authorizationCode = authorizationCodeService.generateAuthorizationCode(clientId, userId, scope);
        const redirectionUrl = `${client.redirectionUrl}?code=${authorizationCode.id}`;

        response.redirect(redirectionUrl);
    });

    return router;
};