module.exports = function () {
    const authorizationCodes = [];

    return {
        generateAuthorizationCode: function (clientId, userId, scope) {
            const id = getAuthorizationCode();
            const exparationDate = Date.now() + 1000 * 60 * 10; // 10 minutes from now
            const authorizationCode = {
                id, clientId, userId, scope, exparationDate
            };

            authorizationCodes.push(authorizationCode);

            return authorizationCode;
        },

        findByClientId: function (clientId) {
            return authorizationCodes.find(function (authorizationCode) {
                return authorizationCode.clientId === clientId;
            });
        }
    };

    function getAuthorizationCode() {
        return authorizationCodes.length + "";
    }
};