module.exports = function () {
    const registeredClients = [];

    return {
        registerClient: function (name, description, redirectionUrl) {
            const id = getClientId();
            const secret = getClientSecret(id);
            const client = {
                id, secret, name, description, redirectionUrl
            };

            registeredClients.push(client);

            return client;
        },

        findById: function (id) {
            return registeredClients.find(function (client) {
                return client.id === id;
            });
        }
    };

    function getClientId() {
        return registeredClients.length + "";
    }

    function getClientSecret(clientId) {
        return clientId + "secret";
    }
};