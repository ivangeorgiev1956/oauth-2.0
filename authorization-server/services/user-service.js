module.exports = function () {
    const users = [];

    return {
        registerUser: function (username, password) {
            const id = getUserId();
            const user = { id, username, password };

            users.push(user);

            return user;
        },

        areCredentialsValid: function (username, password) {
            return users.some(function (user) {
                return user.username === username && user.password === password;
            });
        },

        findById: function (id) {
            return users.find(function (user) {
                return user.id === id;
            });
        }
    };

    function getUserId() {
        return users.length + "";
    }
};