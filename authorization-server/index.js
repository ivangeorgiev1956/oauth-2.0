const express = require("express");
const cors = require("cors");

const createUserService = require("./services/user-service");
const createClientService = require("./services/client-service");
const createAuthorizationCodeService = require("./services/authorization-code-service");

const loginRouter = require("./routers/login-router");
const registrationRouter = require("./routers/registration-router");
const authorizationRouter = require("./routers/authorization-router");

const port = process.env.PORT || 8000;
const app = express();

const userService = createUserService();
const clientService = createClientService();
const authorizationCodeService = createAuthorizationCodeService();

app.set('view engine', 'ejs');

app.use(cors());
app.use(express.urlencoded({ extended: false }));

app.use("/login", loginRouter(userService));
app.use("/register", registrationRouter(clientService));
app.use("/authorize", authorizationRouter(clientService, userService, authorizationCodeService));

app.listen(port, function () {
    console.log("Listening on port " + port);

    userService.registerUser("user", "pass");
    clientService.registerClient("Demo client", "Demo OAuth 2.0 client", "https://wwww.demoapp.com");
});