const express = require("express");
const cors = require("cors");

const port = process.env.PORT || 8002;
const app = express();

app.use(cors());

app.listen(port, function () {
    console.log("Listening on port " + port);
});